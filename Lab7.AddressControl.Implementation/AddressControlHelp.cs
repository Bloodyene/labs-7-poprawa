﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace Lab7.AddressControl
{
    class AddressControlHelp : ViewModel
    {
        private string url;
        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                FirePropertyChanged("url");
            }
        }
        private Command cmd;
        public Command Cmd
        {
            get { return cmd; }
        }
        public AddressControlHelp(Command cmd)
        {
            this.cmd = cmd;
        }
    }
}
