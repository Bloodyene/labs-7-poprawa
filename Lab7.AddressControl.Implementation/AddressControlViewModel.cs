﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TinyMVVMHelper;

namespace Lab7.AddressControl
{
    public class AddressControlViewModel : IAddress
    {
        public event EventHandler<AddressChangedArgs> AddressChanged;
        private AddressControlHelp help;
        private string adres;
        public string Adres
        {
            get { return adres; }
            set { adres = value; }
        }
        private Control control;
        public Control Control
        {
            get { return control; }
        }
        public AddressControlViewModel()
        {
            control = new AddressControlView();
            help = new AddressControlHelp(new Command(FireAddressChanged));
            control.DataContext = help;
        }
        private void FireAddressChanged(object parametr)
        {
            if (AddressChanged != null)
            {
                AddressChangedArgs args = new AddressChangedArgs();
                args.URL = help.Url;

                AddressChanged(this, args);
            }
        }
    }
}
