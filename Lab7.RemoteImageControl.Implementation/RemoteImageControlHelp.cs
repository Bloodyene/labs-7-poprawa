﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using TinyMVVMHelper;

namespace Lab7.RemoteImageControl.Implementation
{
    class RemoteImageControlHelp : ViewModel
    {
        private ImageSource source;
        IAddress address;
        public ImageSource Source
        {
            get { return source; }
            set
            {
                source = value;
                FirePropertyChanged("source");
            }
        }
        public RemoteImageControlHelp(IAddress address)
        {
            this.address = address;
        }
    }
}
