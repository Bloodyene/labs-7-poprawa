﻿using Lab7.AddressControl.Contract;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lab7.RemoteImageControl.Implementation
{
    public class RemoteImageControlViewModel : IRemoteImage
    {
        IAddress address;
        private RemoteImageControlHelp help;
        private Control control;
        public Control Control
        {
            get { return control; }
        }
        public RemoteImageControlViewModel(IAddress address)
        {
            this.address = address;
            control = new RemoteImageControlView();
            help = new RemoteImageControlHelp(address);
            control.DataContext = help;
            address.AddressChanged += OnLoad;
        }
        public void Load(string url)
        {
            OnStart();
            MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri(url)));
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            help.Source = imageSource;
            OnStop();
        }
        void OnLoad(object sender, AddressChangedArgs args)
        {
            Load(args.URL);
        }
        public event EventHandler StartLoading;
        public event EventHandler StopLoadng;
        public void OnStart()
        {
            if (StartLoading != null)
            {
                StartLoading(this, null);
            }
        }

        public void OnStop()
        {
            if (StopLoadng != null)
            {
                StopLoadng(this, null);
            }
        }
    }
}
