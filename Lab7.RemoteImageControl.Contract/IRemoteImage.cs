﻿using Lab7.AddressControl.Contract;
using System;
using System.Windows.Controls;

namespace Lab7.RemoteImageControl.Contract
{
    public interface IRemoteImage
    {
        Control Control { get; }

        void Load(string url);
        event EventHandler StartLoading;
        event EventHandler StopLoadng;
    }
}
