﻿using Lab7.AddressControl.Contract;
using Lab7.Infrastructure;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Windows;
using System.Windows.Controls;
using Lab7.RemoteImageControl.Implementation;

namespace Lab7.Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Start(object sender, EventArgs e)
        {
            MessageBox.Show("Loading Started");
        }
        private void Stop(object sender, EventArgs e)
        {
            MessageBox.Show("Loading Stopped");
        }
        private void Window_Initialized(object sender, EventArgs e)
        {
            var container = Configuration.ConfigureApp();

            var address = container.Resolve<IAddress>();
            Grid.SetRow(address.Control, 0);
            Grid.SetColumn(address.Control, 0);
            var rdef = new RowDefinition();
            rdef.Height = new GridLength(30); /////
            this.Panel.RowDefinitions.Add(rdef);
            this.Panel.Children.Add(address.Control);

            IRemoteImage imagecontrol1 = container.Resolve<IRemoteImage>();
            imagecontrol1.StartLoading += Start;
            imagecontrol1.StopLoadng += Stop;
            Grid.SetRow(imagecontrol1.Control, 1);
            this.Panel.RowDefinitions.Add(new RowDefinition());
            this.Panel.Children.Add(imagecontrol1.Control);

            var imagecontrol2 = container.Resolve<IRemoteImage>();
            imagecontrol2.StartLoading += Start;
            imagecontrol2.StopLoadng += Stop;
            Grid.SetRow(imagecontrol2.Control, 2);
            this.Panel.RowDefinitions.Add(new RowDefinition());
            this.Panel.Children.Add(imagecontrol2.Control);

            var imagecontrol3 = container.Resolve<IRemoteImage>();
            imagecontrol3.StartLoading += Start;
            imagecontrol3.StopLoadng += Stop;
            Grid.SetRow(imagecontrol3.Control, 3);
            this.Panel.RowDefinitions.Add(new RowDefinition());
            this.Panel.Children.Add(imagecontrol3.Control);

            this.Panel.ShowGridLines = true;
        }
    }
}
