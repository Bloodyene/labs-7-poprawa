﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PK.Container;

namespace Lab7.Infrastructure
{
    public class Container : IContainer
    {

        private IDictionary<Type, Type> types = new Dictionary<Type, Type>();
        private IDictionary<Type, Object> obj = new Dictionary<Type, Object>();

        public void Register(Assembly assembly)
        {
            Type[] localTypes;
            localTypes = assembly.GetTypes();

            foreach (Type singleType in localTypes)
            {
                if (!singleType.IsNotPublic)
                {
                    this.Register(singleType);
                }
            }
        }

        public void Register(Type type)
        {
            Type[] localTypes = type.GetInterfaces();

            foreach (Type singleType in localTypes)
            {
                if (types.ContainsKey(singleType))
                {
                    types[singleType] = type;
                }

                else
                {
                    types.Add(singleType, type);
                }
            }
        }

        public void Register<T>(T impl) where T : class
        {
            var interfejsy = impl.GetType().GetInterfaces();

            for (int i = 0; i < interfejsy.Length; i++)
            {
                if (!obj.ContainsKey(interfejsy[i]))
                {
                    obj.Add(interfejsy[i], impl);
                }

                else
                {
                    obj[interfejsy[i]] = impl;
                }
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            Register(provider.Invoke() as T);
        }

        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            if (obj.ContainsKey(type))
            {
                return obj[type];
            }

            else if (!types.ContainsKey(type))
            {
                return null;
            }

            else
            {
                ConstructorInfo[] konstruktory = types[type].GetConstructors();

                foreach (ConstructorInfo item in konstruktory)
                {
                    ParameterInfo[] info = item.GetParameters();

                    if (info.Length == 0)
                    {
                        return Activator.CreateInstance(types[type]);
                    }

                    List<object> lista = new List<object>(info.Length);

                    foreach (ParameterInfo pinfo in info)
                    {
                        var p = Resolve(pinfo.ParameterType);

                        if (p == null)
                        {
                            throw new UnresolvedDependenciesException();
                        }

                        lista.Add(p);
                    }

                    return item.Invoke(lista.ToArray());
                }
            }

            return null;
        }

    }
}
